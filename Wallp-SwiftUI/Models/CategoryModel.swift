//
//  CategoryModel.swift
//  Wallp-SwiftUI
//
//  Created by Андрей Козлов on 20.03.2021.
//

struct CategoryModel {
	let title: String
	let imageUrl: String
	let isFavorite: Bool
	let isPremium: Bool
}

let mockCategories: [CategoryModel] = [
	.init(title: "Абстракция", imageUrl: "asd", isFavorite: true, isPremium: true),
	.init(title: "Пустыня", imageUrl: "asd", isFavorite: false, isPremium: false),
	.init(title: "Растения", imageUrl: "asd", isFavorite: false, isPremium: true),
	.init(title: "Сверху", imageUrl: "asd", isFavorite: false, isPremium: false),
	.init(title: "Вода", imageUrl: "asd", isFavorite: true, isPremium: false),
]
