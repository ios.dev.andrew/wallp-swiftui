//
//  CategoriesView.swift
//  Wallp-SwiftUI
//
//  Created by Андрей Козлов on 20.03.2021.
//

import SwiftUI

struct CategoriesView: View {
	private let categories = mockCategories
	
	private let gridItems = [GridItem(.flexible()), GridItem(.flexible())]
	
	var body: some View {
		NavigationView {
			ScrollView {
				LazyVGrid(columns: gridItems) {
					/*@START_MENU_TOKEN@*/Text("Placeholder")/*@END_MENU_TOKEN@*/
					/*@START_MENU_TOKEN@*/Text("Placeholder")/*@END_MENU_TOKEN@*/
				}
			}
			.navigationTitle("Wallp")
		}
	}
}

struct CategoriesView_Previews: PreviewProvider {
    static var previews: some View {
        CategoriesView()
    }
}
