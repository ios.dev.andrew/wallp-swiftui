//
//  CategoryCell.swift
//  Wallp-SwiftUI
//
//  Created by Андрей Козлов on 20.03.2021.
//

import SwiftUI

struct CategoryCell: View {
	
	let category: CategoryModel
	
    var body: some View {
		ZStack {
			VStack {
				Image(systemName: "cloud.fill")
					.resizable()
					.scaledToFit()
				HStack {
					Text(category.title)
						.font(.system(size: 18, weight: .bold, design: .default))
					if category.isFavorite {
						Image("favorites")
					}
					if category.isPremium {
						Image("premium")
					}
				}
				.padding(.leading, 8)
				.padding(.top, 8)
			}
		}
    }
}

struct CategoryCell_Previews: PreviewProvider {
    static var previews: some View {
		CategoryCell(category: mockCategories[0])
    }
}
