//
//  Wallp_SwiftUIApp.swift
//  Wallp-SwiftUI
//
//  Created by Андрей Козлов on 20.03.2021.
//

import SwiftUI

@main
struct Wallp_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
			CategoriesView()
        }
    }
}
